const form = document.getElementById('form');

const submit = document.getElementById('send');
const authButton = document.getElementById('auth');
const regButton = document.getElementById('register');
const BASE_URL = 'https://shop-itclass.herokuapp.com';
getProducts();


if (submit) {
    submit.addEventListener('click', login);
}

if (authButton) {
    authButton.addEventListener('click', setAuthState);
}

if (regButton) {
    regButton.addEventListener('click', setRegState);
}




let isAuth = true;

const token = localStorage.getItem('token');
if (token) {
    closeModal();
}

function login() {
    if (isAuth) {
        authorization();
    } else {
        registration();
    }
}

function authorization() {
    const formData = new FormData(form);
    const emailPattern = /^([a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$)/gmi;
    const emailIsValid = emailPattern.test(formData.get('email'));
    const password = formData.get('password');
    const passwordIsValid = password.length >= 5;

    if (emailIsValid && passwordIsValid) {
        const user = {
            email: formData.get('email'),
            password: formData.get('password')
        };
        authUser(user).then(data => {
            data.json().then(result => {
                localStorage.setItem('token', result);
                closeModal();
            });
        });
    }
}

function registration() {
    const formData = new FormData(form);
    const emailPattern = /^([a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$)/gmi;
    const emailIsValid = emailPattern.test(formData.get('email'));
    const password = formData.get('password');
    const confirm = formData.get('confirm');
    const passwordIsValid = (password === confirm) && (password.length >= 5);
    if (emailIsValid && passwordIsValid) {
        const user = {
            email: formData.get('email'),
            password: formData.get('password')
        };
        registerUser(user).then(data => {
            data.json().then(result => {
                localStorage.setItem('token', result);
                closeModal();
            });
        });
    }
}

function closeModal() {
    const wrapper = document.getElementsByClassName('wrapper')[0];
    if (wrapper) {
        wrapper.style.display = 'none';
    }
}

function showModal() {
    const wrapper = document.getElementsByClassName('wrapper')[0];
    if (wrapper) {
        wrapper.style.display = 'flex';
    }
}

function registerUser(data) {
    return fetch(BASE_URL.concat('/register'), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

function authUser(data) {
    return fetch(BASE_URL.concat('/auth'), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

const logoutBtn = document.getElementById('logout');
if (logoutBtn) {
    logoutBtn.addEventListener('click', logout);
}


function logout() {
    localStorage.removeItem('token');
    showModal();
}

function setAuthState() {
    if (!isAuth) {
        isAuth = true;
        document.getElementsByClassName('form__title')[0].innerText = 'Авторизация';
        regButton.classList.remove('actions__button_active');
        authButton.classList.add('actions__button_active');
        document.getElementsByClassName('modal__field')[2].style.display = 'none';
    }
}

function setRegState() {
    if (isAuth) {
        isAuth = false;
        document.getElementsByClassName('form__title')[0].innerText = 'Регистрация';
        authButton.classList.remove('actions__button_active');
        regButton.classList.add('actions__button_active');
        document.getElementsByClassName('modal__field')[2].style.display = 'flex';
    }
}


// ------------------------------------------

const name = document.getElementById('name');
const desc = document.getElementById('desc');
const price = document.getElementById('price');

const saveProductBtn = document.getElementById('saveProduct');

saveProductBtn.addEventListener('click', saveProduct);

function saveProduct() {
    const product = {
        name: name.value,
        description: desc.value,
        price: price.value,
        photo: null
    };

    fetch(BASE_URL.concat('/products'), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
    }).then(result => result.json().then(data => {
        clearFields();
    }));
}

function clearFields() {
    name.value = '';
    desc.value = '';
    price.value = '';
}

function getProducts() {
    fetch(BASE_URL.concat('/products'), {
        method: 'GET'
    }).then(result => result.json().then(data => {
        showProducts(data);
    }));
}


const productsArea = document.getElementsByClassName('products')[0];

function drawCard(product) {
    const photo = product.photo ? product.photo : './images/stub.jpg';
    const card = `<div class="card">
                    <img class="card__photo" src="${photo}" alt="">

                    <div class="card__info">
                        <div class="card__info-data">
                            <span class="card__info-data-name">${product.name}</span>
                            <span class="card__info-data-price">${product.price}$</span>
                        </div>

                        <div class="card__info-description">
                            ${product.description}
                        </div>
                    </div>

                    <div class="card__edit">
                        <button class="card__edit-item">Удалить</button>
                        <button class="card__edit-item">Редактировать</button>
                    </div>
                </div>`;
    productsArea.innerHTML += card;
}

function showProducts(products) {
    productsArea.innerHTML = '';
    products.forEach(element => drawCard(element));
}